export interface SearchRequestQuery {
    term?: string;
    level?: number;
    limit?: number;
}

export interface Address {
    id: number;
    obecKod: number;
    obecNazev: string;
    momcKod: number | null;
    momcNazev: string | null;
    mopKod: number | null;
    mopNazev: string | null;
    castObceKod: number | null;
    castObceNazev: string | null;
    uliceKod: number | null;
    uliceNazev: string | null;
    typSo: string | null;
    cisloDomovni: number | null;
    cisloOrientacni: number | null;
    cisloOrientacniZnak: string | null;
    psc: number | null;
    search: string;
    level: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
    gps: {
        latitude: number;
        longitude: number;
    };
    platiOd: string;
    aktualizace: string;
    okresKod: number | null;
    okresNazev: string | null;
    krajKod: number | null;
    krajNazev: string | null;
}

export interface Codebook {
    id: number;
    nazev: string;
    popis: string;
    zkracenyNazev: string;
    zacatekPlatnosti: null;
    konecPlatnosti: null;
}

export interface SimpleCodebook {
    id: number;
    nazev: string;
}

export interface ExtendedAddress extends Address {
    distributorElektrinyKod: string;
    distributorElektrinyId: number;
    distributorPlynuKod: string;
    distributorPlynuId: number;
    typBudovy: Codebook | null;
    isknBudovaId: string;
    druhKonstrukce: Codebook | null;
    pocetBytu: number | null;
    pocetPodlazi: number | null;
    pripojeniKanalizace: Codebook | null;
    pripojeniPlyn: Codebook | null;
    pripojeniVodovod: Codebook | null;
    vybaveniVytahem: Codebook | null;
    zpusobVytapeni: Codebook;
    zastavenaPlocha: number | null;
    obestavenyProstor: number | null;
    podlahovaPlocha: number | null;
    stavebniObjektKod: string | null;
    parcelaKod: number | null;
    zpusobVyuzitiObjektu: Codebook | null;
    createdAt: string | null;
    updatedAt: string | null;
    objectType: SimpleCodebook | null;
    objectTypeBasic: SimpleCodebook | null;
    objectSize: string | null;
    objectSizeSquare: number | null;
}
