import axios from "axios";
import { Address, ExtendedAddress, SearchRequestQuery } from "./types";

const axiosInstance = axios.create({
    baseURL: process.env.SMART_ADDRESS_URL ?? "https://smart-address.cz/api/v1",
    headers: {
        "X-Api-Token": process.env.SMART_ADDRESS_TOKEN,
    },
});

function search(query: SearchRequestQuery): Promise<Address[]> {
    return axiosInstance.get("/search", { params: query }).then((r) => r.data);
}

function get(id: number): Promise<Address> {
    return axiosInstance.get(`/address/${id}`).then((r) => r.data);
}

async function getExtended(id: number): Promise<ExtendedAddress> {
    const [address, extended] = await Promise.all([
        get(id),
        axiosInstance.get(`/address/${id}/extended`).then((r) => r.data),
    ]);

    return {
        ...address,
        ...extended,
    };
}

export const SmartAddress = {
    search,
    get,
    getExtended,
};
