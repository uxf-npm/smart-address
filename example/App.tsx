import React, { useCallback, useState } from "react";
import { SmartAddress } from "../src";

export const App: React.FC = () => {
    const [term, setTerm] = useState("");
    const [address, setAddress] = useState<any>(null);

    const onSearch = useCallback(async () => {
        const address = await SmartAddress.getExtended(1600554782);

        setAddress(address);
    }, [term, setAddress]);

    return (
        <div>
            <div>
                <input
                    type="text"
                    name="search"
                    placeholder="Zadejte hledanou adresu"
                    value={term}
                    onChange={(e) => setTerm(e.target.value ?? "")}
                />
                <button style={{ marginLeft: 8 }} onClick={onSearch}>
                    Hledat
                </button>
            </div>
            <pre>{JSON.stringify(address, null, "    ")}</pre>
        </div>
    );
};
