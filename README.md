# @uxf/smart-address

## Použití balíčku

Environment proměnné:

- `SMART_ADDRESS_URL` (default: https://smart-address.cz/api/v1)
- `SMART_ADDRESS_TOKEN` (required)

```tsx
import { SmartAddress } from "@uxf/smart-address";

const addresses = await SmartAddress.search({ term: "", level: 3, limit: 20});
const address = await SmartAddress.get(/** smart address id */);
const extendedAddress = await SmartAddress.getExtended(/** smart address id */);
```

## Jak vyvíjet?
```bash
git clone git@gitlab.com:uxf-npm/smart-address.git

cd ./smart-address

npm install
```

## Spuštění examplu:

```bash
npm run dev
```

## Vydání balíčku:

Balíček se vydá po mergi do `master` branche.

K publikování nových verzí do npm je opužit balíček [semantic-release](https://github.com/semantic-release/semantic-release) .

### Jak má vypadat commit message?

https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines

| Commit message                                                                                                                                                                                   | Release type               |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |